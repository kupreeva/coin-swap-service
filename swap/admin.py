from django.contrib import admin

# Register your models here.
from swap.models import SwapSession

admin.site.register(SwapSession)