from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from swap.forms import SessionIdForm, AddressForm
from django.contrib.sessions.models import Session
from swap.models import SwapSession
import json
import requests
import datetime
from django.db.models import Sum
from local_settings import *

# Create your views here.


def home(request):
    return render(request, 'home.html', {})


def monitor_balance(request):
    if SwapSession.objects.filter(session_id=request.session.session_key).exists():
        swap_obj = SwapSession.objects.get(session_id=request.session.session_key)
        res = get_balance_of_id(swap_obj.account_id)
        if res > 0:
            swap_obj.incoming_balance = res
            swap_obj.state = 2
            swap_obj.save()
            return render(request, 'generated_id.html', {'balance_not_null': True, 'balance': swap_obj.incoming_balance})
        else:
            time = datetime.datetime.now()
            formated_time = time.strftime("%Y-%m-%d %H:%M:%S")
            #print(formated_time)
            return render(request, 'generated_id.html', {'balance_not_null': False, 'time': formated_time})


def get_balance_of_id(account_id):
    url = url1
    headers = {'content-type': 'text/plain'}
    payload = {
        "method": "getbalance",
        "params": [account_id, 6],
        "jsonrpc": "1.0",
        "id": 0,
    }
    response = requests.post(
        url, data=json.dumps(payload), headers=headers).json()
    return response['result']


def start_swap(request):
    if not request.session.session_key:
        request.session.create()

    session_key = request.session.session_key
    time = datetime.datetime.now()
    formated_time = time.strftime("%Y-%m-%d %H:%M:%S")

    if SwapSession.objects.filter(session_id=session_key).exists():
        swap_obj = SwapSession.objects.get(session_id=session_key)  # doing nothing
    else:
        SwapSession.objects.create(account_id=session_key, session_id=session_key, state=0)
        swap_obj = SwapSession.objects.get(session_id=session_key)

    res = get_balance_of_id(swap_obj.account_id)

    if res > 0 and swap_obj.state < 2:
        swap_obj.incoming_balance = res
        swap_obj.state = 2
        swap_obj.save()

    if swap_obj.state == 2:
        if request.method == "POST":
            form = AddressForm(request.POST)
            if form.is_valid():
                transaction = send_transaction(form.cleaned_data['address'], swap_obj.incoming_balance/10)
                # print(transaction)
                if transaction is not None:
                    swap_obj.addr2 = form.cleaned_data['address']
                    swap_obj.state = 3
                    swap_obj.tx2 = transaction
                    swap_obj.save()
                else:
                    return redirect('error_in_transaction')

    if swap_obj.state < 1:
        addr1 = get_account_addr(swap_obj.account_id)
        swap_obj.addr1 = addr1
        swap_obj.state = 1
        swap_obj.save()
        return render(request, 'generated_id.html', {'session_id': session_key, 'addr1': addr1,'time': formated_time})
    elif swap_obj.state == 1:
        return render(request, 'generated_id.html', {'session_id': session_key, 'addr1':swap_obj.addr1,'time': formated_time})
    elif swap_obj.state == 2:
        balance_com2 = swap_obj.incoming_balance/10
        if request.method == 'GET':
            form = AddressForm()

            return render(request, 'generated_id.html', {'session_id': session_key,
                                                         'addr1': swap_obj.addr1,
                                                         'balance_not_null': True,
                                                         'balance_com': swap_obj.incoming_balance,
                                                         'balance_com2': balance_com2,
                                                         'form': form
                                                         })
        else:
            return redirect('unknown_error')  # неизвестная ошибка
    elif swap_obj.state == 3:
        return render(request, 'success.html', {'balance_com2':swap_obj.incoming_balance/10,
                                                'addr2': swap_obj.addr2,
                                                'tx2': swap_obj.tx2,
                                                })
    else:
        return redirect('invalid_session_id')


def get_account_addr(account_id):
    url = url1
    headers = {'content-type': 'text/plain'}
    payload = {
        "method": "getaccountaddress",
        "params": [account_id],
        "jsonrpc": "1.0",
        "id": 0,
    }
    response = requests.post(
        url, data=json.dumps(payload), headers=headers).json()
    return response['result']


def previous_session(request):

    if request.method == 'GET':
        form = SessionIdForm()
    else:
        form = SessionIdForm(request.POST)
        if form.is_valid():

            if not request.session.session_key:
                request.session.create()

            session_key = form.cleaned_data['session_key']
            if SwapSession.objects.filter(session_id=session_key).exists():
                swap_obj = SwapSession.objects.get(session_id=session_key)
                swap_obj.session_id = request.session.session_key
                swap_obj.save()

                for obj in SwapSession.objects.all():
                    print(obj.session_id)
                    print(obj.account_id)
                    print(obj.addr1)

                return redirect('generated_id')
            else:
                return redirect('invalid_session_id')

    return render(request, "prev_session.html", {'form': form})


def invalid_session_id(request):
    return render(request, 'invalid_session.html', {})


def error_in_transaction(request):
    return render(request, 'error_in_transaction.html', {})


def unknown_error(request):
    return render(request, 'unknown_error.html', {})


def send_transaction(address, amount):
    url = url2
    headers = {'content-type': 'text/plain'}
    payload = {
        "method": "sendtoaddress",
        "params": [address, amount],
        "jsonrpc": "1.0",
        "id": 0,
    }
    response = requests.post(
        url, data=json.dumps(payload), headers=headers).json()
    return response['result']


def stat(request):
    all_objects = SwapSession.objects.all()
    active_swaps = all_objects.filter(state__lt=3).count()
    finished_swaps = all_objects.filter(state__exact=3).count()
    total_coins_swapped = all_objects.filter(state__exact=3).aggregate(Sum('incoming_balance'))
    return render(request, 'stat.html', {'active_swaps': active_swaps,
                                         'finished_swaps': finished_swaps,
                                         'total_coins_swapped': total_coins_swapped['incoming_balance__sum']})


def new_trans(request):
    response = HttpResponseRedirect('/')
    response.delete_cookie('sessionid')
    return response

