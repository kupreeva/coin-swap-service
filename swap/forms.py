from django import forms


class SessionIdForm(forms.Form):
    session_key = forms.CharField(
        required=True,
        label='Enter your previous session id',
        help_text='We will give you a new session id and transfer your data to the new session',
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'required': 'true',
    }))


class AddressForm(forms.Form):
    address = forms.CharField(
        required=True,
        label='Please enter your address in new network',
        help_text='You will receive your SYZ coins at this address',
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'required': 'true',

    }))