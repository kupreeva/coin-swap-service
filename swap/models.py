from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator


# Create your models here.
class SwapSession(models.Model):
    account_id = models.CharField(max_length=100)
    session_id = models.CharField(max_length=100)
    addr1 = models.CharField(max_length=100, blank=True)
    tx1 = models.CharField(max_length=100, blank=True)
    addr2 = models.CharField(max_length=100, blank=True)
    tx2 = models.CharField(max_length=100, blank=True)
    state = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(3)])
    incoming_balance = models.FloatField(blank=True, default=0)

    def __str__(self):
        return self.session_id
