from django.urls import path
from swap import views

urlpatterns = [
    path('previous_session/', views.previous_session, name='prev_session'),
    path('swap_page/', views.start_swap, name='generated_id'),
    path('invalid_session_id/', views.invalid_session_id, name='invalid_session_id'),
    path('error/', views.error_in_transaction, name='error_in_transaction'),
    path('unknown_error/', views.unknown_error, name='unknown_error'),
    path('get_balance_of_id/', views.get_balance_of_id, name='get_balance_of_id'),
    path('monitor_balance/', views.start_swap, name='monitor_balance'),
    path('statistics/', views.stat, name='stat'),
    path('new_transaction/', views.new_trans, name='new_transaction'),
    path('', views.home, name='home')
]
